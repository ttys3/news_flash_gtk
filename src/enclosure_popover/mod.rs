mod enclosure_row;

use self::enclosure_row::EnclosureRow;
use crate::util::BuilderHelper;
use gtk::{ContainerExt, ListBox, Popover};
use news_flash::models::Enclosure;

pub struct EnclosurePopover {
    pub widget: Popover,
    pub list: ListBox,
}

impl EnclosurePopover {
    pub fn new(enclosures: &Vec<Enclosure>) -> Self {
        let builder = BuilderHelper::new("enclosure_popover");
        let popover = builder.get::<Popover>("enclosure_popover");
        let list = builder.get::<ListBox>("enclosure_list");

        let enclosures = enclosures.iter().collect();
        let (image_enc, enclosures) = Self::partition(&enclosures, "image/");
        let (video_enc, enclosures) = Self::partition(&enclosures, "video/");
        let (audio_enc, enclosures) = Self::partition(&enclosures, "audio/");
        Self::fill(&list, &image_enc, "Image");
        Self::fill(&list, &video_enc, "Video");
        Self::fill(&list, &audio_enc, "Audio");
        Self::fill(&list, &enclosures, "Unknown");

        EnclosurePopover { widget: popover, list }
    }

    pub fn widget(&self) -> &Popover {
        &self.widget
    }

    pub fn partition<'a>(
        enclosures: &'a Vec<&Enclosure>,
        mime_prefix: &str,
    ) -> (Vec<&'a Enclosure>, Vec<&'a Enclosure>) {
        enclosures.into_iter().partition::<Vec<_>, _>(|enc| {
            if let Some(mime) = &enc.mime_type {
                mime.starts_with(mime_prefix)
            } else {
                false
            }
        })
    }

    pub fn fill(list: &ListBox, enclosures: &[&Enclosure], title: &str) {
        for (i, enclosure) in enclosures.iter().enumerate() {
            let row = EnclosureRow::new(enclosure, &format!("{} {}", title, i + 1));
            list.add(&row.widget);
        }
    }
}
