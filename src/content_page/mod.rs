mod content_header;
mod error;
mod header_selection;

pub use self::content_header::ContentHeader;
pub use self::header_selection::HeaderSelection;

use self::error::{ContentPageError, ContentPageErrorKind};
use crate::app::Action;
use crate::article_list::{ArticleList, ArticleListModel};
use crate::article_view::ArticleView;
use crate::main_window_state::MainWindowState;
use crate::settings::Settings;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::{FeedListTree, SideBar, TagListModel};
use crate::undo_bar::{UndoActionModel, UndoBar};
use crate::util::{BuilderHelper, Util, CHANNEL_ERROR};
use chrono::{Duration, NaiveDateTime, TimeZone, Utc};
use failure::ResultExt;
use futures::channel::oneshot;
use futures::executor::ThreadPool;
use futures_util::future::FutureExt;
use glib::{clone, Sender};
use gtk::{Box, BoxExt, WidgetExt};
use libhandy::Leaflet;
use log::warn;
use news_flash::models::{
    Article, ArticleFilter, ArticleOrder, Category, CategoryType, Feed, Marked, PluginCapabilities, Read,
    NEWSFLASH_TOPLEVEL,
};
use news_flash::NewsFlash;
use parking_lot::RwLock;
use std::collections::HashSet;
use std::sync::Arc;

pub struct ContentPage {
    pub sidebar: Arc<RwLock<SideBar>>,
    pub article_list: Arc<RwLock<ArticleList>>,
    pub article_view: ArticleView,
    settings: Arc<RwLock<Settings>>,
    prev_settings: RwLock<Option<Settings>>,
    state: Arc<RwLock<MainWindowState>>,
    prev_state: RwLock<Option<MainWindowState>>,
    sender: Sender<Action>,
}

impl ContentPage {
    pub fn new(
        builder: &BuilderHelper,
        state: &Arc<RwLock<MainWindowState>>,
        settings: &Arc<RwLock<Settings>>,
        content_header: &Arc<ContentHeader>,
        sender: Sender<Action>,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
    ) -> Self {
        let feed_list_box = builder.get::<Box>("feedlist_box");
        let article_list_box = builder.get::<Box>("articlelist_box");
        let articleview_box = builder.get::<Box>("articleview_box");

        // workaround
        let minor_leaflet = builder.get::<Leaflet>("minor_leaflet");
        minor_leaflet.set_hexpand(false);

        let sidebar = SideBar::new(state, settings, sender.clone(), features);
        let article_list = Arc::new(RwLock::new(ArticleList::new(
            settings,
            content_header,
            state,
            sender.clone(),
        )));
        let article_view = ArticleView::new(settings, &sender, state);

        feed_list_box.pack_start(&sidebar.read().widget(), false, true, 0);
        article_list_box.pack_start(&article_list.read().widget(), false, true, 0);
        articleview_box.pack_start(&article_view.widget(), false, true, 0);

        let settings = settings.clone();

        ContentPage {
            sidebar,
            article_list,
            article_view,
            settings,
            prev_settings: RwLock::new(None),
            state: state.clone(),
            prev_state: RwLock::new(None),
            sender,
        }
    }

    pub fn clear(&self) {
        self.article_view.close_article();
        self.state.write().set_prefer_scraped_content(false);

        let empty_list_model = ArticleListModel::new(&self.settings.read().get_article_list_order());
        self.article_list
            .write()
            .update(empty_list_model, &self.state, true, self.prev_state.read().clone());

        let feed_tree_model = FeedListTree::new();
        self.sidebar
            .write()
            .update_feedlist(feed_tree_model, &Arc::new(RwLock::new(None)));

        let tag_list_model = TagListModel::new();
        self.sidebar.write().update_taglist(tag_list_model);
        self.sidebar.read().hide_taglist();
        self.prev_settings.write().replace(self.settings.write().clone());
        self.prev_state.write().replace(self.state.write().clone());
    }

    pub fn update_article_view_background(&self) {
        let color = self.article_list.read().get_background_color();
        self.article_view.update_background_color(&color);
    }

    pub fn update_article_list(
        &self,
        news_flash: &Arc<RwLock<Option<NewsFlash>>>,
        undo_bar: &UndoBar,
        threadpool: ThreadPool,
    ) {
        let (sender, receiver) = oneshot::channel::<Result<ArticleListModel, ContentPageErrorKind>>();

        let last_article_in_list_date = self.article_list.read().get_last_row_model().map(|model| model.date);
        // Is this a new list based on changed persitent settings?
        let new_list_because_of_settings = if let Some(prev_settings) = self.prev_settings.read().as_ref() {
            self.settings.read().get_article_list_order() != prev_settings.get_article_list_order()
                || self.settings.read().get_article_list_show_thumbs() != prev_settings.get_article_list_show_thumbs()
        } else {
            false
        };
        // Is this a new list based on changed window state? (e.g. different sidebar selection)
        let new_list_because_of_window_state = if let Some(prev_state) = self.prev_state.read().as_ref() {
            &*self.state.read() != prev_state
        } else {
            false
        };

        let is_new_list = new_list_because_of_settings || new_list_because_of_window_state;
        let prev_state = self.prev_state.read().clone();

        let news_flash = news_flash.clone();
        let window_state = self.state.clone();
        let current_undo_action = undo_bar.get_current_action();
        let processing_undo_actions = undo_bar.processing_actions();
        let settings = self.settings.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let (limit, last_article_date) = if is_new_list {
                    (Some(MainWindowState::page_size()), None)
                } else {
                    if last_article_in_list_date.is_none() {
                        // article list is empty: load default amount of articles to fill it
                        (Some(MainWindowState::page_size()), None)
                    } else {
                        (None, last_article_in_list_date)
                    }
                };

                let mut list_model = ArticleListModel::new(&settings.read().get_article_list_order());
                let mut articles = match Self::load_articles(
                    news_flash,
                    &window_state,
                    &settings,
                    &current_undo_action,
                    &processing_undo_actions,
                    limit,
                    None,
                    last_article_date,
                ) {
                    Ok(articles) => articles,
                    Err(error) => {
                        sender.send(Err(error.kind())).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                let loaded_article_count = articles.len() as i64;
                if loaded_article_count < MainWindowState::page_size() {
                    // article list is not filled all the way up to "page size"
                    // load a few more
                    let mut more_articles = match Self::load_articles(
                        news_flash,
                        &window_state,
                        &settings,
                        &current_undo_action,
                        &processing_undo_actions,
                        Some(MainWindowState::page_size() - loaded_article_count),
                        Some(loaded_article_count),
                        None,
                    ) {
                        Ok(articles) => articles,
                        Err(_error) => {
                            log::warn!("loading more articles to fill up to page size failed");
                            Vec::new()
                        }
                    };
                    articles.append(&mut more_articles);
                }

                let feeds = match news_flash.get_feeds() {
                    Ok((feeds, _)) => feeds,
                    Err(_error) => {
                        sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let articles = articles
                    .drain(..)
                    .filter_map(|article| {
                        feeds
                            .iter()
                            .find(|&f| f.feed_id == article.feed_id)
                            .map(|f| (article, f))
                    })
                    .collect();

                list_model.add(articles);

                sender.send(Ok(list_model)).expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(clone!(
            @weak self.state as window_state,
            @weak self.article_list as article_list => @default-panic, move |res|
        {
            if let Ok(res) = res {
                if let Ok(article_list_model) = res {
                    article_list.write().update(article_list_model, &window_state, is_new_list, prev_state);
                }
            }
        }));

        threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
        self.prev_settings.write().replace(self.settings.write().clone());
        self.prev_state.write().replace(self.state.write().clone());
    }

    pub fn load_more_articles(
        &self,
        news_flash_handle: &Arc<RwLock<Option<NewsFlash>>>,
        window_state: &Arc<RwLock<MainWindowState>>,
        undo_bar: &UndoBar,
        threadpool: ThreadPool,
    ) {
        let (sender, receiver) = oneshot::channel::<Result<ArticleListModel, ContentPageErrorKind>>();

        let relevant_articles_loaded = self
            .article_list
            .read()
            .get_relevant_article_count(window_state.read().get_header_selection());

        let current_undo_action = undo_bar.get_current_action();
        let processing_undo_actions = undo_bar.processing_actions();
        let settings = self.settings.clone();
        let news_flash = news_flash_handle.clone();
        let window_state = window_state.clone();
        let thread_future = async move {
            let mut list_model = ArticleListModel::new(&settings.read().get_article_list_order());

            if let Some(news_flash) = news_flash.read().as_ref() {
                let mut articles = match Self::load_articles(
                    news_flash,
                    &window_state,
                    &settings,
                    &current_undo_action,
                    &processing_undo_actions,
                    Some(MainWindowState::page_size()),
                    Some(relevant_articles_loaded as i64),
                    None,
                ) {
                    Ok(articles) => articles,
                    Err(error) => {
                        sender.send(Err(error.kind())).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let feeds = match news_flash.get_feeds() {
                    Ok((feeds, _)) => feeds,
                    Err(_error) => {
                        sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let articles: Vec<(Article, &Feed)> = articles
                    .drain(..)
                    .filter_map(|article| {
                        feeds
                            .iter()
                            .find(|&f| f.feed_id == article.feed_id)
                            .map(|f| (article, f))
                    })
                    .collect();

                list_model.add(articles);

                sender.send(Ok(list_model)).expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(
            clone!(@weak self.article_list as article_list => @default-panic, move |res| {
                if let Ok(res) = res {
                    if let Ok(article_list_model) = res {
                        article_list.write().add_more_articles(article_list_model);
                    }
                }
            }),
        );

        threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn load_articles(
        news_flash: &NewsFlash,
        window_state: &RwLock<MainWindowState>,
        settings: &Arc<RwLock<Settings>>,
        current_undo_action: &Option<UndoActionModel>,
        processing_undo_actions: &Arc<RwLock<HashSet<UndoActionModel>>>,
        limit: Option<i64>,
        offset: Option<i64>,
        last_article_date: Option<NaiveDateTime>,
    ) -> Result<Vec<Article>, ContentPageError> {
        let limit = match limit {
            Some(limit) => Some(limit),
            None => Some(i64::MAX),
        };
        let unread = match window_state.read().get_header_selection() {
            HeaderSelection::All | HeaderSelection::Marked => None,
            HeaderSelection::Unread => Some(Read::Unread),
        };
        let marked = match window_state.read().get_header_selection() {
            HeaderSelection::All | HeaderSelection::Unread => None,
            HeaderSelection::Marked => Some(Marked::Marked),
        };
        let feed = match &window_state.read().get_sidebar_selection() {
            SidebarSelection::All | SidebarSelection::Category(_, _) | SidebarSelection::Tag(_, _) => None,
            SidebarSelection::Feed(id, _parent_id, _title) => Some(id.clone()),
        };
        let category = match &window_state.read().get_sidebar_selection() {
            SidebarSelection::All | SidebarSelection::Feed(_, _, _) | SidebarSelection::Tag(_, _) => None,
            SidebarSelection::Category(id, _title) => Some(id.clone()),
        };
        let tag = match &window_state.read().get_sidebar_selection() {
            SidebarSelection::All | SidebarSelection::Feed(_, _, _) | SidebarSelection::Category(_, _) => None,
            SidebarSelection::Tag(id, _title) => Some(id.clone()),
        };
        let (older_than, newer_than) = if let Some(last_article_date) = last_article_date {
            let order = settings.read().get_article_list_order();
            match order {
                // +/- 1s to not exclude last article in list
                ArticleOrder::NewestFirst => (
                    None,
                    Some(Utc.from_utc_datetime(&last_article_date) - Duration::seconds(1)),
                ),
                ArticleOrder::OldestFirst => (
                    Some(Utc.from_utc_datetime(&last_article_date) + Duration::seconds(1)),
                    None,
                ),
            }
        } else {
            (None, None)
        };
        let search_term = window_state.read().get_search_term().clone();
        let (feed_blacklist, category_blacklist) = {
            let mut undo_actions = Vec::new();
            let mut feed_blacklist = Vec::new();
            let mut category_blacklist = Vec::new();
            if let Some(current_undo_action) = current_undo_action {
                undo_actions.push(current_undo_action);
            }
            let processing_undo_actions = &*processing_undo_actions.read();
            for processing_undo_action in processing_undo_actions {
                undo_actions.push(&processing_undo_action);
            }
            for undo_action in undo_actions {
                match undo_action {
                    UndoActionModel::DeleteFeed(feed_id, _label) => feed_blacklist.push(feed_id.clone()),
                    UndoActionModel::DeleteCategory(category_id, _label) => {
                        category_blacklist.push(category_id.clone())
                    }
                    UndoActionModel::DeleteTag(_tag_id, _label) => {}
                }
            }

            let feed_blacklist = if feed_blacklist.is_empty() {
                None
            } else {
                Some(feed_blacklist)
            };
            let category_blacklist = if category_blacklist.is_empty() {
                None
            } else {
                Some(category_blacklist)
            };

            (feed_blacklist, category_blacklist)
        };

        let articles = news_flash
            .get_articles(ArticleFilter {
                limit,
                offset,
                order: Some(settings.read().get_article_list_order()),
                unread,
                marked,
                feed,
                feed_blacklist,
                category,
                category_blacklist,
                tag,
                ids: None,
                newer_than,
                older_than,
                search_term,
            })
            .context(ContentPageErrorKind::DataBase)?;

        Ok(articles)
    }

    pub fn update_sidebar(
        &self,
        news_flash: &Arc<RwLock<Option<NewsFlash>>>,
        undo_bar: &UndoBar,
        threadpool: ThreadPool,
        features: &Arc<RwLock<Option<PluginCapabilities>>>,
    ) {
        let (sender, receiver) =
            oneshot::channel::<Result<(i64, FeedListTree, Option<TagListModel>), ContentPageErrorKind>>();

        let news_flash = news_flash.clone();
        let state = self.state.clone();
        let current_undo_action = undo_bar.get_current_action();
        let processing_undo_actions = undo_bar.processing_actions();
        let app_features = features.clone();
        let global_sender = self.sender.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let mut tree = FeedListTree::new();
                let mut tag_list_model: Option<TagListModel> = None;

                let mut categories = match news_flash.get_categories() {
                    Ok(categories) => categories,
                    Err(_error) => {
                        sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                        return;
                    }
                };
                let (feeds, mut mappings) = match news_flash.get_feeds() {
                    Ok(res) => res,
                    Err(_error) => {
                        sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                // collect unread and marked counts
                let feed_count_map = match state.read().get_header_selection() {
                    HeaderSelection::All | HeaderSelection::Unread => match news_flash.unread_count_feed_map() {
                        Ok(res) => res,
                        Err(_error) => {
                            sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                            return;
                        }
                    },
                    HeaderSelection::Marked => match news_flash.marked_count_feed_map() {
                        Ok(res) => res,
                        Err(_error) => {
                            sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                            return;
                        }
                    },
                };

                let mut pending_delte_feeds = HashSet::new();
                let mut pending_delete_categories = HashSet::new();
                let mut pending_delete_tags = HashSet::new();
                if let Some(current_undo_action) = &current_undo_action {
                    match current_undo_action {
                        UndoActionModel::DeleteFeed(id, _label) => pending_delte_feeds.insert(id),
                        UndoActionModel::DeleteCategory(id, _label) => pending_delete_categories.insert(id),
                        UndoActionModel::DeleteTag(id, _label) => pending_delete_tags.insert(id),
                    };
                }
                let processing_undo_actions = &*processing_undo_actions.read();
                for processing_undo_action in processing_undo_actions {
                    match processing_undo_action {
                        UndoActionModel::DeleteFeed(id, _label) => pending_delte_feeds.insert(id),
                        UndoActionModel::DeleteCategory(id, _label) => pending_delete_categories.insert(id),
                        UndoActionModel::DeleteTag(id, _label) => pending_delete_tags.insert(id),
                    };
                }

                // If there are feeds without a category:
                // - create new 'generated' category "Uncategorized"
                // - create mappings for all feeds without category to be children of the newly generated category
                let mut uncategorized_mappings = Util::create_mappings_for_uncategorized_feeds(&feeds, &mappings);
                if !uncategorized_mappings.is_empty() {
                    categories.push(Category {
                        category_id: crate::util::NEWSFLASH_UNCATEGORIZED.clone(),
                        label: "Uncategorized".into(),
                        sort_index: None,
                        parent_id: NEWSFLASH_TOPLEVEL.clone(),
                        category_type: CategoryType::Generated,
                    });
                }
                mappings.append(&mut uncategorized_mappings);

                // feedlist: Categories
                for category in &categories {
                    if pending_delete_categories.contains(&category.category_id) {
                        continue;
                    }

                    let category_item_count = Util::calculate_item_count_for_category(
                        &category.category_id,
                        &categories,
                        &mappings,
                        &feed_count_map,
                        &pending_delte_feeds,
                        &pending_delete_categories,
                    );

                    if tree.add_category(category, category_item_count).is_err() {
                        sender
                            .send(Err(ContentPageErrorKind::SidebarModels))
                            .expect(CHANNEL_ERROR);
                        return;
                    }
                }

                // feedlist: Feeds
                for mapping in &mappings {
                    if pending_delte_feeds.contains(&mapping.feed_id)
                        || pending_delete_categories.contains(&mapping.category_id)
                    {
                        continue;
                    }

                    let feed = match feeds.iter().find(|feed| feed.feed_id == mapping.feed_id) {
                        Some(res) => res,
                        None => {
                            warn!(
                                "Mapping for feed '{}' exists, but can't find the feed itself",
                                mapping.feed_id
                            );
                            Util::send(
                                &global_sender,
                                Action::ErrorSimpleMessage(format!(
                                    "Sidebar: missing feed with id: '{}'",
                                    mapping.feed_id
                                )),
                            );
                            continue;
                        }
                    };

                    let item_count = match feed_count_map.get(&mapping.feed_id) {
                        Some(count) => *count,
                        None => 0,
                    };
                    if tree.add_feed(&feed, &mapping, item_count).is_err() {
                        //sender.send(Err(ContentPageErrorKind::SidebarModels)).expect(CHANNEL_ERROR);
                    }
                }

                // tag list
                let mut support_tags = false;
                if let Some(features) = app_features.read().as_ref() {
                    support_tags = features.contains(PluginCapabilities::SUPPORT_TAGS);
                }

                if support_tags {
                    let mut list = TagListModel::new();
                    let tags = match news_flash.get_tags() {
                        Ok(tags) => tags,
                        Err(_error) => {
                            sender.send(Err(ContentPageErrorKind::DataBase)).expect(CHANNEL_ERROR);
                            return;
                        }
                    };

                    if !tags.is_empty() {
                        for tag in tags {
                            if pending_delete_tags.contains(&tag.tag_id) {
                                continue;
                            }
                            if list.add(&tag).is_err() {
                                sender
                                    .send(Err(ContentPageErrorKind::SidebarModels))
                                    .expect(CHANNEL_ERROR);
                                return;
                            }
                        }
                    }

                    tag_list_model = Some(list);
                }

                //let total_item_count = feed_count_map.iter().map(|(_key, value)| value).sum();
                let total_item_count = Util::calculate_item_count_for_category(
                    &NEWSFLASH_TOPLEVEL,
                    &categories,
                    &mappings,
                    &feed_count_map,
                    &pending_delte_feeds,
                    &pending_delete_categories,
                );

                sender
                    .send(Ok((total_item_count, tree, tag_list_model)))
                    .expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(clone!(
            @weak self.sidebar as sidebar,
            @strong self.sender as sender,
            @strong features => @default-panic, move |res| {
                match res {
                    Ok(res) => {
                        match res {
                            Ok((total_count, feed_list_model, tag_list_model)) => {
                                sidebar.write().update_feedlist(feed_list_model, &features);
                                sidebar.write().update_all(total_count);
                                if let Some(tag_list_model) = tag_list_model {
                                    if tag_list_model.is_empty() {
                                        sidebar.read().hide_taglist();
                                    } else {
                                        sidebar.write().update_taglist(tag_list_model);
                                        sidebar.read().show_taglist();
                                    }
                                } else {
                                    sidebar.read().hide_taglist();
                                }
                            },
                            Err(error) => Util::send(&sender, Action::ErrorSimpleMessage(format!("Failed to update sidebar: '{}'", error)))
                        }
                    },
                    Err(error) => Util::send(&sender, Action::ErrorSimpleMessage(format!("Failed to update sidebar: '{}'", error))),
                }
        }));

        threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
        self.prev_settings.write().replace(self.settings.write().clone());
        self.prev_state.write().replace(self.state.write().clone());
    }

    pub fn article_view_scroll_diff(&self, diff: f64) -> Result<(), ContentPageError> {
        self.article_view
            .animate_scroll_diff(diff)
            .context(ContentPageErrorKind::ArticleView)?;
        Ok(())
    }

    pub fn sidebar_select_next_item(&self) -> Result<(), ContentPageError> {
        self.sidebar
            .read()
            .select_next_item()
            .context(ContentPageErrorKind::SidebarSelection)?;
        Ok(())
    }

    pub fn sidebar_select_prev_item(&self) -> Result<(), ContentPageError> {
        self.sidebar
            .read()
            .select_prev_item()
            .context(ContentPageErrorKind::SidebarSelection)?;
        Ok(())
    }
}
