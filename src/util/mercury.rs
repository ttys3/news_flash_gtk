use crate::util::gtk_util::GTK_RESOURCE_FILE_ERROR;
use crate::util::{UtilError, UtilErrorKind};
use crate::Resources;
use failure::ResultExt;
use glib::{clone, MainLoop, Priority};
use news_flash::models::Url;
use parking_lot::RwLock;
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::str;
use std::sync::Arc;
use webkit2gtk::{Settings, SettingsExt, WebView, WebViewExt};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MercuryOutput {
    pub title: Option<String>,
    pub content: Option<String>,
    pub author: Option<String>,
    pub date_published: Option<String>,
    pub dek: Option<String>,
    pub next_page_url: Option<String>,
    pub url: String,
    pub domain: Option<String>,
    pub excerpt: Option<String>,
    pub word_count: Option<i32>,
    pub direction: Option<String>,
    pub total_pages: Option<i32>,
    pub rendered_pages: Option<i32>,
}

const JS_SNIPPET: &str = r#"
var global_result = "";
var html_content = `
$HTML
`;
console.log(html_content);
Mercury.parse("$URL", {
    html: html_content,
    fetchAllPages: false
    }).then(result => {
        console.log(result);
        global_result = JSON.stringify(result);
        document.title = "ready";
    })
    .catch(err => {
        console.log(err);
        document.title = "error";
    });
    "dummy_result"
"#;

pub struct MercuryParser;

impl MercuryParser {
    pub async fn prep(url: &Url, client: &Client) -> Result<String, UtilError> {
        let result = client.get(url.as_str()).send().await.context(UtilErrorKind::HTTP)?;
        let final_url = result.url().to_string();

        let website_content = result.text().await.context(UtilErrorKind::HTTP)?;

        let snippet = JS_SNIPPET.to_owned();
        //let snippet = snippet.replace("$URL", url.as_str());
        let snippet = snippet.replace("$URL", &final_url);
        let snippet = snippet.replace("$HTML", &website_content);

        let js_resource = Resources::get("mercury.web.js").expect(GTK_RESOURCE_FILE_ERROR);
        let mut js_string = str::from_utf8(js_resource.as_ref())
            .expect(GTK_RESOURCE_FILE_ERROR)
            .to_owned();
        js_string.push_str(&snippet);
        Ok(js_string)
    }

    fn create_webview() -> WebView {
        let settings = Settings::new();
        settings.set_enable_accelerated_2d_canvas(false);
        settings.set_enable_html5_database(false);
        settings.set_enable_html5_local_storage(false);
        settings.set_enable_java(false);
        settings.set_enable_media_stream(false);
        settings.set_enable_page_cache(false);
        settings.set_enable_plugins(false);
        settings.set_enable_smooth_scrolling(false);
        settings.set_enable_javascript(true);
        settings.set_javascript_can_access_clipboard(false);
        settings.set_javascript_can_open_windows_automatically(false);
        settings.set_media_playback_requires_user_gesture(true);
        settings.set_user_agent_with_application_details(Some("NewsFlash"), None);
        settings.set_enable_developer_extras(false);

        let webview = WebView::with_settings(&settings);
        webview
    }

    pub fn parse(javascript: &str) -> Result<MercuryOutput, UtilError> {
        let view = Self::create_webview();
        let wait_loop = Arc::new(MainLoop::new(None, false));
        let value: Arc<RwLock<String>> = Arc::new(RwLock::new(String::new()));

        view.connect_property_title_notify(clone!(@weak wait_loop, @weak value => move |view| {
            if let Some(title) = view.get_title() {
                let title = title.as_str();

                if title == "ready" {
                    let cancellable = gio::Cancellable::new();
                    view.run_javascript("global_result", Some(&cancellable), clone!(@weak wait_loop, @weak value => move |result| {
                        match result {
                            Ok(result) => {
                                if let Some(val) = result.get_value() {
                                    if let Some(context) = result.get_global_context() {
                                        if let Some(string) = val.to_string(&context) {
                                            if string.trim().is_empty() {
                                                log::error!("Empty string: likely a JS error");
                                            } else {
                                                *value.write() = string;
                                            }
                                        } else {
                                            log::error!("Return value is not a string");
                                        }
                                    } else {
                                        log::error!("Failed to get global JS context");
                                    }
                                } else {
                                    log::error!("No return value");
                                }
                            }
                            Err(error) => log::error!("error: '{}'", error),
                        };

                        wait_loop.quit();
                    }));
                } else if title == "error" {
                    log::error!("Mercury JS parsing resulted in error");
                    wait_loop.quit();
                }
            } else {
                log::error!("Failed to get webview title after notify event - this shouldn't happen");
                wait_loop.quit();
            }
        }));

        let cancellable = gio::Cancellable::new();
        view.run_javascript(javascript, Some(&cancellable), |_res| {});

        let timeout_loop_handle = wait_loop.clone();
        let source = glib::source::timeout_source_new_seconds(10, None, Priority::default(), move || {
            if timeout_loop_handle.is_running() {
                log::error!("Parsing took longer than 10 seconds. Aborting process");
                timeout_loop_handle.quit();
            }
            glib::Continue(false)
        });
        source.attach(Some(&wait_loop.get_context()));

        wait_loop.run();
        let res = (*value.read()).clone();

        view.try_close();
        let res = serde_json::from_str(&res).context(UtilErrorKind::Serde)?;
        Ok(res)
    }
}

#[cfg(test)]
mod tests {
    use super::MercuryParser;
    use news_flash::models::Url;

    #[tokio::test]
    async fn parse_phoronix() {
        gtk::init().unwrap();
        let url = Url::parse("https://www.phoronix.com/scan.php?page=news_item&px=GCC-Rust-May-2021-Progress").unwrap();
        let js_string = MercuryParser::prep(&url, &reqwest::Client::new()).await.unwrap();
        let result = MercuryParser::parse(&js_string).unwrap();
        assert_eq!(
            result.title.as_deref(),
            Some("GCC Rust Front-End Continues Advancing With Plans To Eventually Upstream")
        );
    }
}
