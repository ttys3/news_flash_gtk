use chrono::{Duration, Local, NaiveDateTime, TimeZone};
use gio::{Settings as GSettings, SettingsExt as GSettingsExt};

pub struct DateUtil;

impl DateUtil {
    pub fn format(naive_utc: &NaiveDateTime) -> String {
        let date;
        let time;
        let local_datetime = Local.from_utc_datetime(naive_utc);
        let now = Local::now().naive_local();
        let now_date = now.date();
        let naive_local_date = local_datetime.naive_local().date();
        let clock_format = GSettings::new("org.gnome.desktop.interface")
            .get_string("clock-format")
            .expect("Failed to read clock format");

        if clock_format == "12h" {
            time = format!("{}", local_datetime.format("%I:%M %p"));
        } else {
            time = format!("{}", local_datetime.format("%k:%M"));
        }

        if now_date == naive_local_date {
            date = "Today".to_owned();
        } else if now_date - naive_local_date == Duration::days(1) {
            date = "Yesterday".to_owned();
        } else {
            date = format!("{}", local_datetime.format("%e.%m.%Y"));
        }

        format!("{} {}", date, time)
    }
}
