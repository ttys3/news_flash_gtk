use news_flash::models::ArticleOrder;
use serde::{Deserialize, Serialize};
use std::default::Default;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ArticleListSettings {
    pub order: ArticleOrder,
    #[serde(default)]
    pub show_thumbnails: bool,
}

impl Default for ArticleListSettings {
    fn default() -> Self {
        ArticleListSettings {
            order: ArticleOrder::NewestFirst,
            show_thumbnails: true,
        }
    }
}
