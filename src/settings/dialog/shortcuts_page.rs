use crate::app::Action;
use crate::settings::dialog::keybinding_editor::{KeybindState, KeybindingEditor};
use crate::settings::keybindings::Keybindings;
use crate::settings::Settings;
use crate::util::{BuilderHelper, GtkUtil, Util, GTK_BUILDER_ERROR};
use glib::{clone, object::Cast, translate::ToGlib, Sender};
use gtk::{prelude::GtkWindowExtManual, Inhibit, Label, LabelExt, Widget, WidgetExt};
use libhandy::{ActionRow, ActionRowExt, PreferencesPage, PreferencesRowExt, PreferencesWindow};
use parking_lot::RwLock;
use std::sync::Arc;

pub struct ShortcutsPage {
    pub widget: PreferencesPage,
    delete_signal: Arc<RwLock<Option<usize>>>,
    sender: Sender<Action>,
    settings: Arc<RwLock<Settings>>,
    builder: BuilderHelper,
    keybind_signals: Arc<RwLock<Vec<(usize, Widget)>>>,
}

impl ShortcutsPage {
    pub fn new(parent: &PreferencesWindow, sender: &Sender<Action>, settings: &Arc<RwLock<Settings>>) -> Self {
        let builder = BuilderHelper::new("settings_shortcuts");
        let shortcuts_page = builder.get::<PreferencesPage>("shortcuts_page");

        let shortcuts_page = ShortcutsPage {
            widget: shortcuts_page,
            delete_signal: Arc::new(RwLock::new(None)),
            sender: sender.clone(),
            settings: settings.clone(),
            builder,
            keybind_signals: Arc::new(RwLock::new(Vec::new())),
        };
        shortcuts_page.setup_close();
        shortcuts_page.setup_interaction(parent);
        shortcuts_page
    }

    fn setup_close(&self) {
        self.delete_signal.write().replace(
            self.widget
                .connect_delete_event(clone!(
                    @strong self.delete_signal as delete_signal,

                    @strong self.keybind_signals as keybind_signals => @default-panic, move |dialog, _event| {
                        GtkUtil::disconnect_signal(*delete_signal.read(), dialog);
                        delete_signal.write().take();
                        for (id, widget) in &*keybind_signals.read() {
                            GtkUtil::disconnect_signal(Some(*id), widget);
                        }
                        keybind_signals.write().clear();

                        Inhibit(false)
                }))
                .to_glib() as usize,
        );
    }

    fn setup_interaction(&self, parent: &PreferencesWindow) {
        self.setup_keybinding_row(
            "next_article",
            self.settings.read().get_keybind_article_list_next(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "previous_article",
            self.settings.read().get_keybind_article_list_prev(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "toggle_read",
            self.settings.read().get_keybind_article_list_read(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "toggle_marked",
            self.settings.read().get_keybind_article_list_mark(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "open_browser",
            self.settings.read().get_keybind_article_list_open(),
            &self.sender,
            parent,
        );

        self.setup_keybinding_row(
            "next_item",
            self.settings.read().get_keybind_feed_list_next(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "previous_item",
            self.settings.read().get_keybind_feed_list_prev(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "toggle_category_expanded",
            self.settings.read().get_keybind_feed_list_toggle_expanded(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "sidebar_set_read",
            self.settings.read().get_keybind_sidebar_set_read(),
            &self.sender,
            parent,
        );

        self.setup_keybinding_row(
            "shortcuts",
            self.settings.read().get_keybind_shortcut(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "refresh",
            self.settings.read().get_keybind_refresh(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "search",
            self.settings.read().get_keybind_search(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row("quit", self.settings.read().get_keybind_quit(), &self.sender, parent);

        self.setup_keybinding_row(
            "all_articles",
            self.settings.read().get_keybind_all_articles(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "only_unread",
            self.settings.read().get_keybind_only_unread(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "only_starred",
            self.settings.read().get_keybind_only_starred(),
            &self.sender,
            parent,
        );

        self.setup_keybinding_row(
            "scroll_up",
            self.settings.read().get_keybind_article_view_up(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "scroll_down",
            self.settings.read().get_keybind_article_view_down(),
            &self.sender,
            parent,
        );
        self.setup_keybinding_row(
            "scrap_content",
            self.settings.read().get_keybind_article_view_scrap(),
            &self.sender,
            parent,
        );
    }

    fn setup_keybinding_row(
        &self,
        id: &str,
        keybinding: Option<String>,
        sender: &Sender<Action>,
        parent: &PreferencesWindow,
    ) {
        let label = self.builder.get::<Label>(&format!("{}_label", id));
        Self::keybind_label_text(keybinding, &label);
        let row_name = format!("{}_row", id);
        let row = self.builder.get::<ActionRow>(&row_name);
        let id = id.to_owned();

        let info_text = row.get_title().expect(GTK_BUILDER_ERROR);
        self.keybind_signals.write().push((
            row.connect_activated(clone!(
                @weak parent as dialog,
                @weak self.settings as settings,
                @strong sender,
                @strong id => @default-panic, move |row|
            {
                if row.get_widget_name().as_str() == row_name {
                    let editor = KeybindingEditor::new(&dialog, &info_text);
                    editor.widget().present();
                    editor.widget().connect_destroy(clone!(
                        @weak label,
                        @weak settings,
                        @strong sender,
                        @strong id => @default-panic, move |_dialog|
                    {
                        let _settings = settings.clone();
                        match &*editor.keybinding.read() {
                            KeybindState::Canceled | KeybindState::Illegal => {}
                            KeybindState::Disabled => {
                                if Keybindings::write_keybinding(&id, None, &settings).is_ok() {
                                    Self::keybind_label_text(None, &label);
                                } else {
                                    Util::send(
                                        &sender,
                                        Action::ErrorSimpleMessage("Failed to write keybinding.".to_owned()),
                                    );
                                }
                            }
                            KeybindState::Enabled(keybind) => {
                                if Keybindings::write_keybinding(&id, Some(keybind.clone()), &settings).is_ok()
                                {
                                    Self::keybind_label_text(Some(keybind.clone()), &label);
                                } else {
                                    Util::send(
                                        &sender,
                                        Action::ErrorSimpleMessage("Failed to write keybinding.".to_owned()),
                                    );
                                }
                            }
                        }
                    }));
                }
            }))
            .to_glib() as usize,
            row.upcast::<Widget>(),
        ));
    }

    fn keybind_label_text(keybinding: Option<String>, label: &Label) {
        let label_text = match keybinding {
            Some(keybinding) => {
                label.set_sensitive(true);
                Keybindings::parse_shortcut_string(&keybinding)
                    .expect("Failed parsing saved shortcut. This should never happen!")
            }
            None => {
                label.set_sensitive(false);
                "Disabled".to_owned()
            }
        };
        label.set_label(&label_text);
    }
}
