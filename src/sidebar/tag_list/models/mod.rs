mod error;
mod tag;

use self::error::{TagListModelError, TagListModelErrorKind};
use crate::sidebar::SidebarIterateItem;
use diffus::{edit::Edit, Diffable};
use news_flash::models::{Tag, TagID};
use std::collections::HashSet;
pub use tag::TagListTagModel;

#[derive(Clone, Debug)]
pub struct TagListModel {
    models: Vec<TagListTagModel>,
    tags: HashSet<TagID>,
}

impl TagListModel {
    pub fn new() -> Self {
        TagListModel {
            models: Vec::new(),
            tags: HashSet::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.tags.is_empty()
    }

    pub fn add(&mut self, tag: &Tag) -> Result<(), TagListModelError> {
        if self.tags.contains(&tag.tag_id) {
            return Err(TagListModelErrorKind::AlreadyExists.into());
        }
        let model = TagListTagModel::new(tag);
        self.tags.insert(model.id.clone());
        self.models.push(model);
        Ok(())
    }

    pub fn generate_diff<'a>(&'a self, other: &'a TagListModel) -> Edit<'_, Vec<TagListTagModel>> {
        self.models.diff(&other.models)
    }

    pub fn sort(&mut self) {
        self.models.sort_by(|a, b| a.cmp(b));
    }

    pub fn calculate_selection(&self, selected_index: i32) -> Option<(usize, &TagListTagModel)> {
        self.models
            .iter()
            .enumerate()
            .find(|(index, _item)| index == &(selected_index as usize))
    }

    pub fn calculate_next_item(&self, selected_index: i32) -> SidebarIterateItem {
        match self.calculate_selection(selected_index + 1) {
            None => SidebarIterateItem::SelectAll,
            Some((_index, model)) => SidebarIterateItem::SelectTagList(model.id.clone()),
        }
    }

    pub fn calculate_prev_item(&self, selected_index: i32) -> SidebarIterateItem {
        match self.calculate_selection(selected_index - 1) {
            None => SidebarIterateItem::FeedListSelectLastItem,
            Some((_index, model)) => SidebarIterateItem::SelectTagList(model.id.clone()),
        }
    }

    pub fn first(&self) -> Option<TagListTagModel> {
        self.models.first().cloned()
    }

    pub fn last(&self) -> Option<TagListTagModel> {
        self.models.last().cloned()
    }
}
