use open;
use std::fs::File;
use std::path::PathBuf;
use std::sync::Arc;
use std::time;

use crate::i18n::{i18n, i18n_f};
use crate::util::UtilError;
use futures::channel::oneshot::{self, Sender as OneShotSender};
use futures::executor::{ThreadPool, ThreadPoolBuilder};
use futures::FutureExt;
use gdk::{WindowExt, WindowState};
use gio::{
    prelude::ApplicationExtManual,
    subclass::{ApplicationImpl, ObjectImpl, ObjectSubclass},
    ApplicationExt, ApplicationFlags, Notification, NotificationPriority, ThemedIcon,
};
use glib::translate::ToGlibPtr;
use glib::{
    clone, glib_object_impl, glib_object_subclass, glib_wrapper, object::Cast, source::Continue, subclass,
    translate::ToGlib, Receiver, Sender,
};
use glib::{translate::FromGlibPtrFull, StaticType};
use gtk::{
    prelude::GtkWindowExtManual, subclass::prelude::GtkApplicationImpl, ButtonExt, DialogExt, EntryExt,
    FileChooserAction, FileChooserDialog, FileChooserExt, FileChooserNative, FileFilter, GtkApplicationExt,
    GtkWindowExt, NativeDialogExt, ResponseType, Widget, WidgetExt,
};
use lazy_static::lazy_static;
use log::{error, info, warn};
use news_flash::models::{
    ArticleID, Category, CategoryID, FatArticle, FavIcon, Feed, FeedID, LoginData, PasswordLogin, PluginCapabilities,
    PluginID, TagID, Thumbnail, Url,
};
use news_flash::{NewsFlash, NewsFlashError, NewsFlashErrorKind};
use parking_lot::RwLock;
use tokio::runtime::Runtime;
use tokio::time::{error::Elapsed, timeout, Duration};

use crate::about_dialog::NewsFlashAbout;
use crate::add_dialog::{AddCategory, AddPopover};
use crate::article_list::{MarkUpdate, ReadUpdate};
use crate::article_view::ArticleView;
use crate::config::{APP_ID, VERSION};
use crate::content_page::HeaderSelection;
use crate::discover::DiscoverDialog;
use crate::login_screen::{PasswordLoginPrevPage, WebLoginPrevPage};
use crate::main_window::MainWindow;
use crate::rename_dialog::RenameDialog;
use crate::settings::{NewsFlashShortcutWindow, Settings, SettingsDialog, UserDataSize};
use crate::sidebar::{models::SidebarSelection, FeedListDndAction};
use crate::undo_bar::UndoActionModel;
use crate::util::{FileUtil, GtkUtil, MercuryParser, Util, CHANNEL_ERROR, RUNTIME_ERROR};

lazy_static! {
    pub static ref CONFIG_DIR: PathBuf = glib::get_user_config_dir()
        .expect("Failed to find the config dir")
        .join("news-flash");
    pub static ref DATA_DIR: PathBuf = glib::get_user_data_dir()
        .expect("Failed to find the data dir")
        .join("news-flash");
    pub static ref WEBKIT_DATA_DIR: PathBuf = DATA_DIR.join("Webkit");
}

#[derive(Debug, Clone)]
pub struct NotificationCounts {
    pub new: i64,
    pub unread: i64,
}

#[derive(Debug)]
pub enum Action {
    ShowNotification(NotificationCounts),
    ErrorSimpleMessage(String),
    Error(String, NewsFlashError),
    UndoableAction(UndoActionModel),
    LoadFavIcon((FeedID, OneShotSender<Option<FavIcon>>)),
    LoadThumbnail((ArticleID, OneShotSender<Option<Thumbnail>>)),
    ShowWelcomePage,
    ShowContentPage(PluginID),
    ShowPasswordLogin(PluginID, Option<PasswordLogin>, PasswordLoginPrevPage),
    ShowOauthLogin(PluginID, WebLoginPrevPage),
    ShowResetPage,
    ShowDiscoverDialog,
    ShowSettingsWindow,
    ShowShortcutWindow,
    ShowAboutWindow,
    CancelReset,
    UpdateLogin,
    Login(LoginData),
    ResetAccount,
    ResetAccountError(NewsFlashError),
    ScheduleSync,
    Sync,
    InitSync,
    MarkArticleRead(ReadUpdate),
    MarkArticle(MarkUpdate),
    ToggleArticleRead,
    ToggleArticleMarked,
    UpdateSidebar,
    UpdateArticleList,
    ArticleListIndexSelected(i32),
    LoadMoreArticles,
    SidebarSelection(SidebarSelection),
    SelectNextArticle,
    SelectPrevArticle,
    HeaderSelection(HeaderSelection),
    UpdateArticleHeader,
    ShowArticle(ArticleID),
    RedrawArticle,
    CloseArticle,
    SearchTerm(String),
    SetSidebarRead,
    AddDialog,
    AddFeed((Url, Option<String>, Option<AddCategory>)),
    AddCategory(String),
    AddTag(String, String),
    RenameFeedDialog(FeedID, CategoryID),
    RenameFeed((Feed, String)),
    RenameCategoryDialog(CategoryID),
    RenameCategory((Category, String)),
    DeleteSidebarSelection,
    DeleteFeed(FeedID),
    DeleteCategory(CategoryID),
    DeleteTag(TagID),
    TagArticle(ArticleID, TagID),
    UntagArticle(ArticleID, TagID),
    DragAndDrop(FeedListDndAction),
    ExportArticle,
    StartGrabArticleContent,
    MercuryGrabArticleContent(FatArticle, Option<Url>, Vec<Url>),
    FinishGrabArticleContent(Option<FatArticle>),
    ImportOpml,
    ExportOpml,
    QueueQuit,
    ForceQuit,
    SetOfflineMode(bool),
    IgnoreTLSErrors,
    OpenSelectedArticle,
    OpenUrlInDefaultBrowser(String),
    QueryDiskSpace(OneShotSender<UserDataSize>),
    ClearCache(OneShotSender<()>),
}
pub struct AppPrivate {
    window: RwLock<Option<Arc<MainWindow>>>,
    sender: Sender<Action>,
    receiver: RwLock<Option<Receiver<Action>>>,
    news_flash: Arc<RwLock<Option<NewsFlash>>>,
    settings: Arc<RwLock<Settings>>,
    sync_source_id: RwLock<Option<u32>>,
    threadpool: ThreadPool,
    icon_threadpool: ThreadPool,
    shutdown_in_progress: Arc<RwLock<bool>>,
    start_headless: Arc<RwLock<bool>>,
    features: Arc<RwLock<Option<PluginCapabilities>>>,
}

impl ObjectSubclass for AppPrivate {
    const NAME: &'static str = "AppPrivate";
    type ParentType = gtk::Application;
    type Instance = subclass::simple::InstanceStruct<Self>;
    type Class = subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn new() -> Self {
        info!("NewsFlash {} ({})", VERSION, APP_ID);

        let shutdown_in_progress = Arc::new(RwLock::new(false));
        let start_headless = Arc::new(RwLock::new(false));
        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RwLock::new(Some(r));

        let cpu_cores = num_cpus::get();
        let max_size = 64;
        let min_size = 8;
        let pool_size = if cpu_cores > max_size {
            max_size
        } else if cpu_cores < min_size {
            min_size
        } else {
            cpu_cores
        };
        let threadpool = ThreadPoolBuilder::new()
            .pool_size(pool_size)
            .create()
            .expect("Failed to init thread pool");
        let icon_threadpool = ThreadPoolBuilder::new()
            .pool_size(16)
            .create()
            .expect("Failed to init thread pool");

        let news_flash = Arc::new(RwLock::new(None));
        let features = Arc::new(RwLock::new(None));
        let settings = Settings::open().expect("Failed to access settings file");
        let settings = Arc::new(RwLock::new(settings));

        let app = Self {
            window: RwLock::new(None),
            sender,
            receiver,
            news_flash,
            settings,
            sync_source_id: RwLock::new(None),
            threadpool,
            icon_threadpool,
            shutdown_in_progress,
            start_headless,
            features,
        };

        if let Ok(news_flash_lib) = NewsFlash::try_load(&DATA_DIR, &CONFIG_DIR) {
            info!("Successful load from config");

            if let Ok(features) = news_flash_lib.features() {
                app.features.write().replace(features);
            }
            app.news_flash.write().replace(news_flash_lib);
            Util::send(&app.sender, Action::ScheduleSync);
        } else {
            warn!("No account configured");
        }

        app
    }
}

impl ObjectImpl for AppPrivate {
    glib_object_impl!();
}

impl GtkApplicationImpl for AppPrivate {}

impl ApplicationImpl for AppPrivate {
    fn activate(&self, _app: &gio::Application) {
        if let Some(window) = self.window.read().as_ref() {
            window.widget.show();
            window.widget.present();

            // Workaround SINGLE selection mode of listbox
            window.content_page.sidebar.read().feed_list.read().on_window_show();
            window.content_page.sidebar.read().tag_list.read().on_window_show();
            return;
        }

        self.window.write().replace(Arc::new(MainWindow::new(
            &self.settings,
            self.sender.clone(),
            self.shutdown_in_progress.clone(),
            &self.features,
        )));

        let app = self.as_app();
        app.add_window(&self.get_main_window().widget);
        self.get_main_window().init(
            &self.news_flash,
            self.threadpool.clone(),
            &self.features,
            &self.settings,
        );

        if *self.start_headless.read() {
            self.get_main_window().widget.hide();
        } else {
            self.get_main_window().widget.show();
            self.get_main_window().widget.present();
        }

        // Workaround SINGLE selection mode of listbox
        self.get_main_window()
            .content_page
            .sidebar
            .read()
            .feed_list
            .read()
            .on_window_show();
        self.get_main_window()
            .content_page
            .sidebar
            .read()
            .tag_list
            .read()
            .on_window_show();

        let receiver = self.receiver.write().take().expect(CHANNEL_ERROR);
        receiver.attach(None, move |action| app.process_action(action));
    }
}

impl AppPrivate {
    fn get_main_window(&self) -> Arc<MainWindow> {
        self.window.read().clone().expect("window not initialized")
    }

    fn as_app(&self) -> App {
        ObjectSubclass::get_instance(self)
            .downcast::<App>()
            .expect("Failed to cast to App")
    }

    fn show_notification(&self, counts: NotificationCounts) {
        // don't notify when window is visible and focused
        if self.get_main_window().widget.is_visible() {
            if let Some(gdk_window) = self.get_main_window().widget.get_window() {
                if gdk_window.get_state().contains(WindowState::FOCUSED) {
                    return;
                }
            }
        }

        if counts.new > 0 && counts.unread > 0 {
            let summary = i18n("New Articles");

            let message = if counts.new == 1 {
                i18n_f("There is 1 new article ({} unread)", &[&counts.unread.to_string()])
            } else {
                i18n_f(
                    "There are {} new articles ({} unread)",
                    &[&counts.new.to_string(), &counts.unread.to_string()],
                )
            };

            let notification = Notification::new(&summary);
            notification.set_body(Some(&message));
            notification.set_priority(NotificationPriority::Normal);
            notification.set_icon(&ThemedIcon::new(APP_ID));

            self.as_app().send_notification(Some("newsflash_sync"), &notification);
        }
    }

    fn login(&self, data: LoginData) {
        let id = match &data {
            LoginData::OAuth(oauth) => oauth.id.clone(),
            LoginData::Password(pass) => pass.id.clone(),
            LoginData::None(id) => id.clone(),
        };
        let news_flash_lib = match NewsFlash::new(&DATA_DIR, &CONFIG_DIR, &id) {
            Ok(news_flash) => news_flash,
            Err(error) => {
                match &data {
                    LoginData::OAuth(_) => self.get_main_window().oauth_login_page.show_error(error),
                    LoginData::Password(_) => self.get_main_window().password_login_page.show_error(error),
                    LoginData::None(_) => {}
                }
                return;
            }
        };

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let news_flash = self.news_flash.clone();
        let global_sender = self.sender.clone();
        let settings = self.settings.clone();
        let data_clone = data.clone();
        let app_features = self.features.clone();
        let thread_future = async move {
            let result = Runtime::new()
                .expect(RUNTIME_ERROR)
                .block_on(news_flash_lib.login(data_clone, &Util::build_client(&settings)));
            match result {
                Ok(()) => {
                    // query features
                    let features = news_flash_lib.features();
                    let features = match features {
                        Ok(features) => features,
                        Err(error) => {
                            sender.send(Err(error)).expect(CHANNEL_ERROR);
                            return;
                        }
                    };
                    app_features.write().replace(features);
                    // create main obj
                    news_flash.write().replace(news_flash_lib);
                    // show content page
                    Util::send(&global_sender, Action::ShowContentPage(id));
                    // schedule initial sync
                    Util::send(&global_sender, Action::InitSync);

                    sender.send(Ok(())).expect(CHANNEL_ERROR);
                }
                Err(error) => {
                    error!("Login failed! Plguin: {}, Error: {}", id, error);
                    sender.send(Err(error)).expect(CHANNEL_ERROR);
                }
            }
        };

        let main_window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @weak main_window => @default-panic, move |res|
        {
            match res {
                Ok(Err(error)) => {
                    match data {
                        LoginData::OAuth(_) => {
                            main_window.oauth_login_page.show_error(error);
                        }
                        LoginData::Password(_) => {
                            main_window.password_login_page.show_error(error);
                        }
                        LoginData::None(_) => {
                            // NOTHING
                        }
                    }
                },
                Ok(Ok(())) => {
                    main_window.update_features();
                }
                _ => {}
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn update_login(&self) {
        self.get_main_window().hide_error_bar();
        if let Some(news_flash) = self.news_flash.read().as_ref() {
            if let Some(login_data) = news_flash.get_login_data() {
                match login_data {
                    LoginData::None(_id) => error!("updating login for local should never happen!"),
                    LoginData::Password(password_data) => {
                        let plugin_id = password_data.id.clone();
                        Util::send(
                            &self.sender,
                            Action::ShowPasswordLogin(
                                plugin_id.clone(),
                                Some(password_data),
                                PasswordLoginPrevPage::Content(plugin_id),
                            ),
                        );
                    }
                    LoginData::OAuth(oauth_data) => {
                        let plugin_id = oauth_data.id.clone();
                        Util::send(
                            &self.sender,
                            Action::ShowOauthLogin(plugin_id.clone(), WebLoginPrevPage::Content(plugin_id)),
                        );
                    }
                }
            }
        }
    }

    fn reset_account(&self) {
        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.logout(&Util::build_client(&settings)));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let main_window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @weak main_window,
            @weak self.news_flash as news_flash,
            @weak self.features as features,
            @strong self.sender as sender => @default-panic, move |res| match res
        {
            Ok(Ok(())) => {
                news_flash.write().take();
                main_window.content_page.clear();
                main_window.content_header.show_article(None, None, &Arc::new(RwLock::new(None)), &features);
                Util::send(&sender, Action::ShowWelcomePage);
            }
            Ok(Err(error)) => {
                Util::send(&sender, Action::ResetAccountError(error));
            }
            Err(error) => {
                log::error!("Unexpected error completing account reset: {}", error);
                Util::send(&sender, Action::ResetAccountError(NewsFlashErrorKind::Unknown.into()));
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn schedule_sync(&self) {
        GtkUtil::remove_source(*self.sync_source_id.read());
        let sync_interval = self.settings.read().get_sync_interval();
        if let Some(sync_interval) = sync_interval.to_seconds() {
            self.sync_source_id.write().replace(
                glib::timeout_add_seconds_local(
                    sync_interval,
                    clone!(@strong self.sender as sender => @default-panic, move || {
                        Util::send(&sender, Action::Sync);
                        Continue(true)
                    }),
                )
                .to_glib(),
            );
        } else {
            self.sync_source_id.write().take();
        }
    }

    fn sync(&self) {
        let (sender, receiver) = oneshot::channel::<Result<i64, NewsFlashError>>();
        self.get_main_window().content_header.start_sync();

        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.sync(&Util::build_client(&settings)));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let main_window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @strong self.news_flash as news_flash,
            @weak main_window.content_header as content_header,
            @strong self.sender as sender => @default-panic, move |res|
        {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let unread_count = match news_flash.unread_count_all() {
                    Ok(unread_count) => unread_count,
                    Err(_) => 0,
                };
                match res {
                    Ok(Ok(new_article_count)) => {
                        content_header.finish_sync();
                        Util::send(&sender, Action::UpdateSidebar);
                        Util::send(&sender, Action::UpdateArticleList);
                        let counts = NotificationCounts {
                            new: new_article_count,
                            unread: unread_count,
                        };
                        Util::send(&sender, Action::ShowNotification(counts));
                    }
                    Ok(Err(error)) => {
                        content_header.finish_sync();
                        Util::send(&sender, Action::Error("Failed to sync.".to_owned(), error));
                    }
                    Err(_) => {}
                }
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn init_sync(&self) {
        let (sender, receiver) = oneshot::channel::<Result<i64, NewsFlashError>>();
        self.get_main_window().content_header.start_sync();

        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.initial_sync(&Util::build_client(&settings)));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let main_window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @strong self.news_flash as news_flash,
            @weak main_window.content_header as content_header,
            @strong self.sender as sender => @default-panic, move |res|
        {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let unread_count = match news_flash.unread_count_all() {
                    Ok(unread_count) => unread_count,
                    Err(_) => 0,
                };
                match res {
                    Ok(Ok(new_article_count)) => {
                        content_header.finish_sync();
                        Util::send(&sender, Action::UpdateSidebar);
                        Util::send(&sender, Action::UpdateArticleList);
                        let counts = NotificationCounts {
                            new: new_article_count,
                            unread: unread_count,
                        };
                        Util::send(&sender, Action::ShowNotification(counts));
                    }
                    Ok(Err(error)) => {
                        content_header.finish_sync();
                        Util::send(&sender, Action::Error("Failed to sync.".to_owned(), error));
                    }
                    Err(_) => {}
                }
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn load_favicon(&self, feed_id: FeedID, oneshot_sender: OneShotSender<Option<FavIcon>>) {
        let news_flash = self.news_flash.clone();
        let global_sender = self.sender.clone();
        let settings = self.settings.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let favicon = match Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.get_icon_info(&feed_id, &Util::build_client(&settings)))
                {
                    Ok(favicon) => Some(favicon),
                    Err(_) => {
                        warn!("Failed to load favicon for feed: '{}'", feed_id);
                        None
                    }
                };
                oneshot_sender.send(favicon).expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(&global_sender, Action::ErrorSimpleMessage(message));
            }
        };

        self.icon_threadpool.spawn_ok(thread_future);
    }

    fn load_thumbnail(&self, article_id: ArticleID, oneshot_sender: OneShotSender<Option<Thumbnail>>) {
        let news_flash = self.news_flash.clone();
        let global_sender = self.sender.clone();
        let settings = self.settings.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let thumbnail = match Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.get_article_thumbnail(&article_id, &Util::build_client(&settings)))
                {
                    Ok(thumbnail) => Some(thumbnail),
                    Err(_) => {
                        log::debug!("Failed to load thumbnail for article: '{}'", article_id);
                        None
                    }
                };
                oneshot_sender.send(thumbnail).expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(&global_sender, Action::ErrorSimpleMessage(message));
            }
        };

        self.icon_threadpool.spawn_ok(thread_future);
    }

    fn mark_article_read(&self, update: ReadUpdate) {
        self.get_main_window()
            .content_page
            .article_list
            .write()
            .set_article_row_state(&update.article_id, Some(update.read), None);

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let article_id_vec = vec![update.article_id.clone()];
        let read_status = update.read;
        let global_sender = self.sender.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                sender
                    .send(
                        Runtime::new()
                            .expect(RUNTIME_ERROR)
                            .block_on(news_flash.set_article_read(
                                &article_id_vec,
                                read_status,
                                &Util::build_client(&settings),
                            )),
                    )
                    .expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(&global_sender, Action::ErrorSimpleMessage(message));
            }
        };

        let window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @strong self.sender as global_sender,
            @strong self.news_flash as news_flash,
            @strong self.features as features,
            @weak window.content_page as content_page,
            @weak window.content_header as content_header => @default-panic, move |res|
        {
            match res {
                Ok(Ok(())) => {}
                Ok(Err(error)) => {
                    let message = format!("Failed to mark article read: '{}'", update.article_id);
                    error!("{}", message);
                    Util::send(&global_sender, Action::Error(message, error));
                    Util::send(&global_sender, Action::UpdateArticleList);
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(&global_sender, Action::ErrorSimpleMessage(message));
                    Util::send(&global_sender, Action::UpdateArticleList);
                }
            };

            Util::send(&global_sender, Action::UpdateSidebar);
            let (visible_article, visible_article_enclosures) = content_page.article_view.get_visible_article();
            if let Some(mut visible_article) = visible_article {
                if visible_article.article_id == update.article_id {
                    visible_article.unread = update.read;
                    content_header.show_article(Some(&visible_article), visible_article_enclosures.as_ref(), &news_flash, &features);
                    content_page
                        .article_view
                        .update_visible_article(Some(visible_article.unread), None);
                }
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn mark_article(&self, update: MarkUpdate) {
        self.get_main_window()
            .content_page
            .article_list
            .write()
            .set_article_row_state(&update.article_id, None, Some(update.marked));

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let news_flash = self.news_flash.clone();
        let article_id_vec = vec![update.article_id.clone()];
        let mark_status = update.marked;
        let global_sender = self.sender.clone();
        let settings = self.settings.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                sender
                    .send(
                        Runtime::new()
                            .expect(RUNTIME_ERROR)
                            .block_on(news_flash.set_article_marked(
                                &article_id_vec,
                                mark_status,
                                &Util::build_client(&settings),
                            )),
                    )
                    .expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(&global_sender, Action::ErrorSimpleMessage(message));
            }
        };

        let window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @strong self.sender as global_sender,
            @strong self.news_flash as news_flash,
            @strong self.features as features,
            @weak window.content_header as content_header,
            @weak window.content_page as content_page => @default-panic, move |res|
        {
            match res {
                Ok(Ok(())) => {}
                Ok(Err(error)) => {
                    let message = format!("Failed to star article: '{}'", update.article_id);
                    error!("{}", message);
                    Util::send(&global_sender, Action::Error(message, error));
                    Util::send(&global_sender, Action::UpdateArticleList);
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(&global_sender, Action::ErrorSimpleMessage(message));
                    Util::send(&global_sender, Action::UpdateArticleList);
                }
            };

            Util::send(&global_sender, Action::UpdateSidebar);
            let (visible_article, visible_article_enclosures) = content_page.article_view.get_visible_article();
            if let Some(mut visible_article) = visible_article {
                if visible_article.article_id == update.article_id {
                    visible_article.marked = update.marked;
                    content_header.show_article(Some(&visible_article), visible_article_enclosures.as_ref(), &news_flash, &features);
                    content_page
                        .article_view
                        .update_visible_article(None, Some(visible_article.marked));
                }
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn toggle_article_read(&self) {
        let selected_article = self
            .get_main_window()
            .content_page
            .article_list
            .read()
            .get_selected_article_model();
        if let Some(selected_article) = selected_article {
            let update = ReadUpdate {
                article_id: selected_article.id.clone(),
                read: selected_article.read.invert(),
            };
            Util::send(&self.sender, Action::MarkArticleRead(update));
        }
    }

    fn toggle_article_marked(&self) {
        let selected_article = self
            .get_main_window()
            .content_page
            .article_list
            .read()
            .get_selected_article_model();
        if let Some(selected_article) = selected_article {
            let update = MarkUpdate {
                article_id: selected_article.id.clone(),
                marked: selected_article.marked.invert(),
            };
            Util::send(&self.sender, Action::MarkArticle(update));
        }
    }

    fn spawn_shortcut_window(&self) {
        let dialog = NewsFlashShortcutWindow::new(&self.get_main_window().widget, &*self.settings.read());
        dialog.widget.present();
    }

    fn spawn_about_window(&self) {
        let dialog = NewsFlashAbout::new(&self.get_main_window().widget);
        dialog.widget.present();
    }

    fn spawn_settings_window(&self) {
        let dialog = SettingsDialog::new(
            &self.get_main_window().widget,
            &self.sender,
            &self.settings,
            &self.news_flash,
        );
        dialog.widget.present();
    }

    fn spawn_discover_dialog(&self) {
        let dialog = DiscoverDialog::new(
            &self.get_main_window().widget,
            &self.sender,
            &self.settings,
            &self.news_flash,
            self.threadpool.clone(),
        );
        dialog.widget.present();
    }

    fn add_feed_dialog(&self) {
        if let Some(news_flash) = self.news_flash.read().as_ref() {
            let error_message = "Failed to add feed".to_owned();
            let add_button = self
                .get_main_window()
                .content_page
                .sidebar
                .read()
                .footer
                .add_button
                .clone();

            let categories = match news_flash.get_categories() {
                Ok(categories) => categories,
                Err(error) => {
                    error!("{}", error_message);
                    Util::send(&self.sender, Action::Error(error_message, error));
                    return;
                }
            };

            let _dialog = AddPopover::new(
                &self.sender,
                &add_button.upcast::<Widget>(),
                categories,
                self.threadpool.clone(),
                &self.settings,
                &self.features,
            );
        }
    }

    fn add_feed(&self, feed_url: Url, title: Option<String>, category: Option<AddCategory>) {
        info!("add feed '{}'", feed_url);

        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let global_sender = self.sender.clone();
        let thread_future = async move {
            let error_message = "Failed to add feed".to_owned();
            if let Some(news_flash) = news_flash.read().as_ref() {
                let category_id = match category {
                    Some(category) => match category {
                        AddCategory::New(category_title) => {
                            let client = Util::build_client(&settings);
                            let add_category_future = news_flash.add_category(&category_title, None, None, &client);
                            let category = match Runtime::new().expect(RUNTIME_ERROR).block_on(add_category_future) {
                                Ok(category) => category,
                                Err(error) => {
                                    error!("{}: Can't add Category", error_message);
                                    Util::send(&global_sender, Action::Error(error_message, error));
                                    return;
                                }
                            };
                            Some(category.category_id)
                        }
                        AddCategory::Existing(category_id) => Some(category_id),
                    },
                    None => None,
                };

                let client = Util::build_client(&settings);
                let add_feed_future = news_flash
                    .add_feed(&feed_url, title, category_id, &client)
                    .map(|result| match result {
                        Ok(_) => {}
                        Err(error) => {
                            error!("{}: Can't add Feed", error_message);
                            Util::send(&global_sender, Action::Error(error_message.clone(), error));
                        }
                    });
                Runtime::new().expect(RUNTIME_ERROR).block_on(add_feed_future);
                Util::send(&global_sender, Action::UpdateSidebar);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(&global_sender, Action::ErrorSimpleMessage(message));
            }
        };
        self.threadpool.spawn_ok(thread_future);
    }

    fn add_category(&self, title: String) {
        info!("add category '{}'", title);

        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let global_sender = self.sender.clone();
        let thread_future = async move {
            let error_message = "Failed to add category".to_owned();
            if let Some(news_flash) = news_flash.read().as_ref() {
                let client = Util::build_client(&settings);
                let add_category_future =
                    news_flash
                        .add_category(&title, None, None, &client)
                        .map(|result| match result {
                            Ok(_) => {}
                            Err(error) => {
                                error!("{}: Can't add Category", error_message);
                                Util::send(&global_sender, Action::Error(error_message.clone(), error));
                            }
                        });
                Runtime::new().expect(RUNTIME_ERROR).block_on(add_category_future);
                Util::send(&global_sender, Action::UpdateSidebar);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(&global_sender, Action::ErrorSimpleMessage(message));
            }
        };
        self.threadpool.spawn_ok(thread_future);
    }

    fn add_tag(&self, color: String, title: String) {
        info!("add tag '{}'", title);

        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let global_sender = self.sender.clone();
        let thread_future = async move {
            let error_message = "Failed to add tag".to_owned();
            if let Some(news_flash) = news_flash.read().as_ref() {
                let client = Util::build_client(&settings);
                let add_tag_future =
                    news_flash
                        .add_tag(&title, Some(color), None, &client)
                        .map(|result| match result {
                            Ok(_) => {}
                            Err(error) => {
                                error!("{}: Can't add Tag", error_message);
                                Util::send(&global_sender, Action::Error(error_message.clone(), error));
                            }
                        });
                Runtime::new().expect(RUNTIME_ERROR).block_on(add_tag_future);
                Util::send(&global_sender, Action::UpdateSidebar);
                Util::send(&global_sender, Action::UpdateArticleHeader);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(&global_sender, Action::ErrorSimpleMessage(message));
            }
        };
        self.threadpool.spawn_ok(thread_future);
    }

    fn rename_feed_dialog(&self, feed_id: FeedID, parent_id: CategoryID) {
        if let Some(news_flash) = self.news_flash.read().as_ref() {
            let (feeds, _mappings) = match news_flash.get_feeds() {
                Ok(result) => result,
                Err(error) => {
                    let message = "Failed to laod list of feeds.".to_owned();
                    Util::send(&self.sender, Action::Error(message, error));
                    return;
                }
            };

            let feed = match feeds.iter().find(|f| f.feed_id == feed_id).cloned() {
                Some(feed) => feed,
                None => {
                    let message = format!("Failed to find feed '{}'", feed_id);
                    Util::send(&self.sender, Action::ErrorSimpleMessage(message));
                    return;
                }
            };

            let dialog = RenameDialog::new(
                &self.get_main_window().widget,
                &SidebarSelection::Feed(feed_id, parent_id, feed.label.clone()),
            );

            dialog.rename_button.connect_clicked(clone!(
                @weak dialog.rename_entry as rename_entry,
                @weak dialog.dialog as rename_dialog,
                @strong self.sender as sender => @default-panic, move |_button|
            {
                let new_label = rename_entry.get_text().to_owned();
                if new_label.is_empty() {
                    Util::send(
                        &sender,
                        Action::ErrorSimpleMessage("No valid title to rename feed.".to_owned()),
                    );
                    rename_dialog.emit_close();
                    return;
                }

                let feed = feed.clone();
                Util::send(&sender, Action::RenameFeed((feed, new_label)));
                rename_dialog.emit_close();
            }));
        }
    }

    fn rename_feed(&self, feed: Feed, new_title: String) {
        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let sender = self.sender.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.rename_feed(
                    &feed,
                    &new_title,
                    &Util::build_client(&settings),
                )) {
                    Util::send(&sender, Action::Error("Failed to rename feed.".to_owned(), error));
                }
            }

            Util::send(&sender, Action::UpdateArticleList);
            Util::send(&sender, Action::UpdateSidebar);
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn rename_category_dialog(&self, category_id: CategoryID) {
        if let Some(news_flash) = self.news_flash.read().as_ref() {
            let categories = match news_flash.get_categories() {
                Ok(categories) => categories,
                Err(error) => {
                    let message = "Failed to load list of categories.".to_owned();
                    Util::send(&self.sender, Action::Error(message, error));
                    return;
                }
            };

            let category = match categories.iter().find(|c| c.category_id == category_id).cloned() {
                Some(category) => category,
                None => {
                    let message = format!("Failed to find category '{}'", category_id);
                    Util::send(&self.sender, Action::ErrorSimpleMessage(message));
                    return;
                }
            };

            let dialog = RenameDialog::new(
                &self.get_main_window().widget,
                &SidebarSelection::Category(category_id, category.label.clone()),
            );

            dialog.rename_button.connect_clicked(clone!(
                @weak dialog.dialog as rename_dialog,
                @weak dialog.rename_entry as rename_entry,
                @strong self.sender as sender => @default-panic, move |_button|
            {
                let new_label = rename_entry.get_text().to_owned();
                if new_label.is_empty() {
                    Util::send(
                        &sender,
                        Action::ErrorSimpleMessage("No valid title to rename category.".to_owned()),
                    );
                    rename_dialog.emit_close();
                    return;
                }

                let category = category.clone();
                Util::send(&sender, Action::RenameCategory((category, new_label)));
                rename_dialog.emit_close();
            }));
        }
    }

    fn rename_category(&self, category: Category, new_title: String) {
        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let sender = self.sender.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                if let Err(error) = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.rename_category(&category, &new_title, &Util::build_client(&settings)))
                {
                    Util::send(&sender, Action::Error("Failed to rename category.".to_owned(), error));
                }
            }

            Util::send(&sender, Action::UpdateSidebar);
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn delete_selection(&self) {
        let selection = self.get_main_window().content_page.sidebar.read().get_selection();
        let undo_action = match selection {
            SidebarSelection::All => {
                warn!("Trying to delete item while 'All Articles' is selected");
                None
            }
            SidebarSelection::Feed(feed_id, _parent_id, label) => Some(UndoActionModel::DeleteFeed(feed_id, label)),
            SidebarSelection::Category(category_id, label) => Some(UndoActionModel::DeleteCategory(category_id, label)),
            SidebarSelection::Tag(tag_id, label) => Some(UndoActionModel::DeleteTag(tag_id, label)),
        };
        if let Some(undo_action) = undo_action {
            Util::send(&self.sender, Action::UndoableAction(undo_action));
        }
    }

    fn delete_feed(&self, feed_id: FeedID) {
        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let sender = self.sender.clone();
        let processing_actions = self.get_main_window().undo_bar.processing_actions();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let (feeds, _mappings) = match news_flash.get_feeds() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(&sender, Action::Error("Failed to delete feed.".to_owned(), error));
                        processing_actions
                            .write()
                            .remove(&UndoActionModel::DeleteFeed(feed_id.clone(), "".into()));
                        return;
                    }
                };

                if let Some(feed) = feeds.iter().find(|f| f.feed_id == feed_id).cloned() {
                    info!("delete feed '{}' (id: {})", feed.label, feed.feed_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_feed(&feed, &Util::build_client(&settings)))
                    {
                        Util::send(&sender, Action::Error("Failed to delete feed.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to delete feed: feed with id '{}' not found.", feed_id);
                    Util::send(&sender, Action::ErrorSimpleMessage(message));
                    error!("feed not found: {}", feed_id);
                }
            }
            processing_actions
                .write()
                .remove(&UndoActionModel::DeleteFeed(feed_id.clone(), "".into()));
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn delete_category(&self, category_id: CategoryID) {
        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let sender = self.sender.clone();
        let processing_actions = self.get_main_window().undo_bar.processing_actions();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let categories = match news_flash.get_categories() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(&sender, Action::Error("Failed to delete category.".to_owned(), error));
                        processing_actions
                            .write()
                            .remove(&UndoActionModel::DeleteCategory(category_id.clone(), "".into()));
                        return;
                    }
                };

                if let Some(category) = categories.iter().find(|c| c.category_id == category_id).cloned() {
                    info!("delete category '{}' (id: {})", category.label, category.category_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_category(&category, true, &Util::build_client(&settings)))
                    {
                        Util::send(&sender, Action::Error("Failed to delete category.".to_owned(), error));
                    }
                } else {
                    let message = format!(
                        "Failed to delete category: category with id '{}' not found.",
                        category_id
                    );
                    Util::send(&sender, Action::ErrorSimpleMessage(message));
                    error!("category not found: {}", category_id);
                }
            }
            processing_actions
                .write()
                .remove(&UndoActionModel::DeleteCategory(category_id.clone(), "".into()));
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn delete_tag(&self, tag_id: TagID) {
        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let sender = self.sender.clone();
        let processing_actions = self.get_main_window().undo_bar.processing_actions();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let tags = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(&sender, Action::Error("Failed to delete tag.".to_owned(), error));
                        processing_actions
                            .write()
                            .remove(&UndoActionModel::DeleteTag(tag_id.clone(), "".into()));
                        return;
                    }
                };

                if let Some(tag) = tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                    info!("delete tag '{}' (id: {})", tag.label, tag.tag_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_tag(&tag, &Util::build_client(&settings)))
                    {
                        Util::send(&sender, Action::Error("Failed to delete tag.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to delete tag: tag with id '{}' not found.", tag_id);
                    Util::send(&sender, Action::ErrorSimpleMessage(message));
                    error!("tag not found: {}", tag_id);
                }
            }
            processing_actions
                .write()
                .remove(&UndoActionModel::DeleteTag(tag_id.clone(), "".into()));
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn tag_article(&self, article_id: ArticleID, tag_id: TagID) {
        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let sender = self.sender.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let tags = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(&sender, Action::Error("Failed to tag article.".to_owned(), error));
                        return;
                    }
                };
                let article = match news_flash.get_article(&article_id) {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(
                            &sender,
                            Action::Error("Failed to tag article. Article not found.".to_owned(), error),
                        );
                        return;
                    }
                };

                if let Some(tag) = tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                    info!("tag article '{}' with '{}'", article_id, tag.tag_id);
                    if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.tag_article(
                        &article,
                        &tag,
                        &Util::build_client(&settings),
                    )) {
                        Util::send(&sender, Action::Error("Failed to tag article.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to tag article: tag with id '{}' not found.", tag_id);
                    Util::send(&sender, Action::ErrorSimpleMessage(message));
                    error!("tag not found: {}", tag_id);
                }
            }
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn untag_article(&self, article_id: ArticleID, tag_id: TagID) {
        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let sender = self.sender.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let tags = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(&sender, Action::Error("Failed to untag article.".to_owned(), error));
                        return;
                    }
                };
                let article = match news_flash.get_article(&article_id) {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(
                            &sender,
                            Action::Error("Failed to untag article. Article not found.".to_owned(), error),
                        );
                        return;
                    }
                };

                if let Some(tag) = tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                    info!("untag article '{}' with '{}'", article_id, tag.tag_id);
                    if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.untag_article(
                        &article,
                        &tag,
                        &Util::build_client(&settings),
                    )) {
                        Util::send(&sender, Action::Error("Failed to untag article.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to tag article: untag with id '{}' not found.", tag_id);
                    Util::send(&sender, Action::ErrorSimpleMessage(message));
                    error!("tag not found: {}", tag_id);
                }
            }
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn drag_and_drop(&self, action: FeedListDndAction) {
        let news_flash = self.news_flash.clone();
        let settings = self.settings.clone();
        let sender = self.sender.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let runtime = Runtime::new().expect(RUNTIME_ERROR);
                match action {
                    FeedListDndAction::MoveCategory(category_id, parent_id, _sort_index) => {
                        if let Err(error) = runtime.block_on(news_flash.move_category(
                            &category_id,
                            &parent_id,
                            &Util::build_client(&settings),
                        )) {
                            Util::send(&sender, Action::Error("Failed to move category.".to_owned(), error));
                        }
                    }
                    FeedListDndAction::MoveFeed(feed_id, from_id, to_id, _sort_index) => {
                        if let Err(error) = runtime.block_on(news_flash.move_feed(
                            &feed_id,
                            &from_id,
                            &to_id,
                            &Util::build_client(&settings),
                        )) {
                            Util::send(&sender, Action::Error("Failed to move feed.".to_owned(), error));
                        }
                    }
                }
            }
            Util::send(&sender, Action::UpdateSidebar);
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn export_article(&self) {
        let (sender, receiver) = oneshot::channel::<()>();

        if let (Some(article), _article_enclosures) =
            self.get_main_window().content_page.article_view.get_visible_article()
        {
            let dialog = FileChooserDialog::with_buttons(
                Some("Export Article"),
                Some(&self.get_main_window().widget),
                FileChooserAction::Save,
                &[("Cancel", ResponseType::Cancel), ("Save", ResponseType::Ok)],
            );

            let filter = FileFilter::new();
            filter.add_pattern("*.html");
            filter.add_mime_type("text/html");
            filter.set_name(Some("HTML"));
            dialog.add_filter(&filter);
            dialog.set_filter(&filter);
            if let Some(title) = &article.title {
                dialog.set_current_name(&format!("{}.html", title.replace("/", "_")));
            } else {
                dialog.set_current_name("Article.html");
            }

            if let ResponseType::Ok = dialog.run() {
                self.get_main_window().content_header.start_more_actions_spinner();

                let news_flash = self.news_flash.clone();
                let global_sender = self.sender.clone();
                let filename = match dialog.get_filename() {
                    Some(filename) => filename,
                    None => {
                        Util::send(&self.sender, Action::ErrorSimpleMessage("No filename set.".to_owned()));
                        return;
                    }
                };
                let window_state = self.get_main_window().state.clone();
                let settings = self.settings.clone();
                let thread_future = async move {
                    if let Some(news_flash) = news_flash.read().as_ref() {
                        let article = if window_state.read().get_offline() {
                            article
                        } else {
                            match Runtime::new().expect(RUNTIME_ERROR).block_on(
                                news_flash.article_download_images(&article.article_id, &Util::build_client(&settings)),
                            ) {
                                Ok(article) => article,
                                Err(error) => {
                                    Util::send(
                                        &global_sender,
                                        Action::Error("Failed to downlaod article images.".to_owned(), error),
                                    );
                                    sender.send(()).expect(CHANNEL_ERROR);
                                    return;
                                }
                            }
                        };

                        sender.send(()).expect(CHANNEL_ERROR);

                        let (feeds, _) = match news_flash.get_feeds() {
                            Ok(feeds) => feeds,
                            Err(error) => {
                                Util::send(
                                    &global_sender,
                                    Action::Error("Failed to load feeds from db.".to_owned(), error),
                                );
                                return;
                            }
                        };
                        let feed = match feeds.iter().find(|&f| f.feed_id == article.feed_id) {
                            Some(feed) => feed,
                            None => {
                                Util::send(
                                    &global_sender,
                                    Action::ErrorSimpleMessage("Failed to find specific feed.".to_owned()),
                                );
                                return;
                            }
                        };
                        let html = ArticleView::build_article_static(
                            "article",
                            &article,
                            &feed.label,
                            &settings,
                            None,
                            None,
                            true,
                        );
                        if FileUtil::write_text_file(&filename, &html).is_err() {
                            Util::send(
                                &global_sender,
                                Action::ErrorSimpleMessage("Failed to write OPML data to disc.".to_owned()),
                            );
                        }
                    }
                };

                let window = self.get_main_window();
                let glib_future = receiver.map(
                    clone!(@weak window.content_header as content_header => @default-panic, move |_res| {
                        content_header.stop_more_actions_spinner();
                    }),
                );

                self.threadpool.spawn_ok(thread_future);
                Util::glib_spawn_future(glib_future);
            }
            dialog.emit_close();
        }
    }

    fn start_grab_article_content(&self) {
        let (sender, receiver) = oneshot::channel::<Result<Result<FatArticle, NewsFlashError>, Elapsed>>();

        if let (Some(article), _article_enclosures) =
            self.get_main_window().content_page.article_view.get_visible_article()
        {
            // Article already scraped: just swap to scraped content
            if article.scraped_content.is_some() {
                Util::send(&self.sender, Action::FinishGrabArticleContent(Some(article)));
                return;
            }

            self.get_main_window()
                .state
                .write()
                .started_scraping_article(&article.article_id);
            self.get_main_window().content_header.start_scrap_content_spinner();

            let news_flash = self.news_flash.clone();
            let settings = self.settings.clone();
            let article_id = article.article_id.clone();
            let thread_future = async move {
                if let Some(news_flash) = news_flash.read().as_ref() {
                    let client = Util::build_client(&settings);
                    let news_flash_future = news_flash.article_scrap_content(&article_id, &client);
                    let article = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(async { timeout(Duration::from_secs(60), news_flash_future).await });
                    sender.send(article).expect(CHANNEL_ERROR);
                }
            };

            let glib_future = receiver.map(clone!(
                @strong self.sender as sender,
                @strong article as oritinal_article => @default-panic, move |res| match res
            {
                Ok(Ok(Ok(article))) => {
                    Util::send(&sender, Action::FinishGrabArticleContent(Some(article)));
                }
                Ok(Ok(Err(_error))) => {
                    Util::send(&sender, Action::MercuryGrabArticleContent(oritinal_article, None, vec![]));
                }
                Ok(Err(_error)) => {
                    log::warn!("Internal scraper elapsed");
                    Util::send(&sender, Action::MercuryGrabArticleContent(oritinal_article, None, vec![]));
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(&sender, Action::ErrorSimpleMessage(message));
                    Util::send(&sender, Action::FinishGrabArticleContent(None));
                }
            }));

            self.threadpool.spawn_ok(thread_future);
            Util::glib_spawn_future(glib_future);
        }
    }

    fn mercury_grab_article_contnet(
        &self,
        article: FatArticle,
        next_page_url: Option<Url>,
        mut previous_urls: Vec<Url>,
    ) {
        let (sender, receiver) = oneshot::channel::<Result<String, UtilError>>();
        let settings = self.settings.clone();
        let article_url = if let Some(next_page_url) = &next_page_url {
            log::info!("parsing next page: '{}'", next_page_url);
            next_page_url.clone()
        } else {
            article.url.clone().unwrap()
        };
        let thread_future = async move {
            let js_string = Runtime::new()
                .expect(RUNTIME_ERROR)
                .block_on(MercuryParser::prep(&article_url, &Util::build_client(&settings)));
            sender.send(js_string).expect(CHANNEL_ERROR);
        };

        let glib_future = receiver.map(clone!(
            @strong self.sender as sender,
            @strong article as original_article => @default-panic, move |res| match res
        {
            Ok(Ok(js_string)) => {
                match MercuryParser::parse(&js_string) {
                    Ok(mercury_output) => {
                        let mut updated_article = original_article;
                        if let Some(content) = mercury_output.content {
                            if let Some(scraped_content) = updated_article.scraped_content.as_mut() {
                                scraped_content.push_str(&content);
                            } else {
                                updated_article.scraped_content = Some(content);
                            }

                            if let Some(next_page) = mercury_output.next_page_url {
                                if let Ok(next_page) = Url::parse(&next_page) {
                                    if previous_urls.contains(&next_page) {
                                        // we already did parse this url -> probably done
                                        Util::send(&sender, Action::FinishGrabArticleContent(Some(updated_article)));
                                    } else {
                                        if let Ok(current_url) = Url::parse(&mercury_output.url) {
                                            previous_urls.push(current_url);
                                        }
                                        Util::send(&sender, Action::MercuryGrabArticleContent(updated_article, Some(next_page), previous_urls));
                                    }
                                }
                            } else {
                                Util::send(&sender, Action::FinishGrabArticleContent(Some(updated_article)));
                            }
                        } else {
                            let message = format!("Parsing sucessful, but no content could be extracted");
                            error!("{}", message);
                            Util::send(&sender, Action::ErrorSimpleMessage(message));
                            // if this was the first page that failed finish with 'None', pass the article otherwise
                            let article = if next_page_url.is_some() { Some(updated_article) } else { None };
                            Util::send(&sender, Action::FinishGrabArticleContent(article));
                        }
                    }
                    Err(error) => {
                        let message = format!("Download of article failed: {}", error);
                        error!("{}", message);
                        Util::send(&sender, Action::ErrorSimpleMessage(message));
                        Util::send(&sender, Action::FinishGrabArticleContent(None));
                    }
                }
            }
            Ok(Err(error)) => {
                let message = format!("Download of article failed: {}", error);
                error!("{}", message);
                Util::send(&sender, Action::ErrorSimpleMessage(message));
                Util::send(&sender, Action::FinishGrabArticleContent(None));
            }
            Err(error) => {
                let message = format!("Sender error: {}", error);
                error!("{}", message);
                Util::send(&sender, Action::ErrorSimpleMessage(message));
                Util::send(&sender, Action::FinishGrabArticleContent(None));
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn finish_grab_article_content(&self, article: Option<FatArticle>) {
        self.get_main_window().content_header.stop_scrap_content_spinner();

        if let Some(news_flash) = self.news_flash.read().as_ref() {
            if let Some(updated_article) = &article {
                news_flash.update_external_scraped_content(updated_article).unwrap();
            }
        }

        if let Some(article) = &article {
            self.get_main_window()
                .state
                .write()
                .finished_scraping_article(&article.article_id);
        }

        let scraped_article_id = article.map(|a| a.article_id);
        let visible_article_id = self
            .get_main_window()
            .content_page
            .article_view
            .get_visible_article()
            .0
            .map(|a| a.article_id);

        if let (Some(scraped_article_id), Some(visible_article_id)) = (&scraped_article_id, &visible_article_id) {
            if scraped_article_id == visible_article_id {
                self.get_main_window()
                    .show_article(scraped_article_id.clone(), &self.news_flash, &self.features);
            }
        } else if scraped_article_id.is_none() {
            self.get_main_window()
                .content_header
                .update_scrape_content_button_state(false);
        }
    }

    fn import_opml(&self) {
        let (sender, receiver) = oneshot::channel::<()>();

        let dialog = FileChooserNative::new(
            Some(&i18n("Import OPML")),
            Some(&self.get_main_window().widget),
            FileChooserAction::Open,
            Some(&i18n("Import")),
            Some(&i18n("Cancel")),
        );

        let filter = FileFilter::new();
        filter.add_pattern("*.OPML");
        filter.add_pattern("*.opml");
        filter.add_mime_type("application/xml");
        filter.add_mime_type("text/xml");
        filter.add_mime_type("text/x-opml");
        filter.set_name(Some("OPML"));
        dialog.add_filter(&filter);
        dialog.set_filter(&filter);

        if let ResponseType::Accept = dialog.run() {
            let window = self.get_main_window();
            let glib_future = receiver.map(clone!(
                @strong self.sender as sender,
                @weak window => @default-panic, move |res| match res
            {
                Ok(()) => {
                    window.content_header.finish_sync();
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(&sender, Action::ErrorSimpleMessage(message));
                }
            }));

            Util::glib_spawn_future(glib_future);

            if let Some(filename) = dialog.get_filename() {
                if let Ok(opml_content) = FileUtil::read_text_file(&filename) {
                    let news_flash = self.news_flash.clone();
                    let global_sender = self.sender.clone();
                    let settings = self.settings.clone();
                    let thread_future = async move {
                        if let Some(news_flash) = news_flash.read().as_ref() {
                            let result = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.import_opml(
                                &opml_content,
                                false,
                                &Util::build_client(&settings),
                            ));

                            if let Err(error) = result {
                                Util::send(
                                    &global_sender,
                                    Action::Error("Failed to import OPML.".to_owned(), error),
                                );
                            } else {
                                Util::send(&global_sender, Action::UpdateSidebar);
                            }

                            sender.send(()).expect(CHANNEL_ERROR);
                        }
                    };
                    self.threadpool.spawn_ok(thread_future);
                    self.get_main_window().content_header.start_sync();
                } else {
                    Util::send(
                        &self.sender,
                        Action::ErrorSimpleMessage("Failed to read content of OPML file.".to_owned()),
                    );
                }
            }
        }
    }

    fn export_opml(&self) {
        let dialog = FileChooserDialog::with_buttons(
            Some(&i18n("Export OPML")),
            Some(&self.get_main_window().widget),
            FileChooserAction::Save,
            &[
                (&i18n("Cancel"), ResponseType::Cancel),
                (&i18n("Save"), ResponseType::Ok),
            ],
        );

        let filter = FileFilter::new();
        filter.add_pattern("*.OPML");
        filter.add_pattern("*.opml");
        filter.add_mime_type("application/xml");
        filter.add_mime_type("text/xml");
        filter.add_mime_type("text/x-opml");
        filter.set_name(Some("OPML"));
        dialog.add_filter(&filter);
        dialog.set_filter(&filter);
        dialog.set_current_name("NewsFlash.OPML");

        if let ResponseType::Ok = dialog.run() {
            if let Some(news_flash) = self.news_flash.read().as_ref() {
                let opml = match news_flash.export_opml() {
                    Ok(opml) => opml,
                    Err(error) => {
                        Util::send(
                            &self.sender,
                            Action::Error("Failed to get OPML data.".to_owned(), error),
                        );
                        return;
                    }
                };

                // Format XML
                if let Ok(tree) = xmltree::Element::parse(opml.as_bytes()) {
                    let write_config = xmltree::EmitterConfig::new().perform_indent(true);
                    if let Some(filename) = dialog.get_filename() {
                        if let Ok(file) = File::create(&filename) {
                            if !tree.write_with_config(file, write_config).is_ok() {
                                Util::send(
                                    &self.sender,
                                    Action::ErrorSimpleMessage("Failed to write OPML data to disc.".to_owned()),
                                );
                            }
                        } else {
                            Util::send(
                                &self.sender,
                                Action::ErrorSimpleMessage("Failed create file.".to_owned()),
                            );
                        }
                    }
                } else {
                    Util::send(
                        &self.sender,
                        Action::ErrorSimpleMessage("Failed to parse OPML data for formatting.".to_owned()),
                    );
                }
            }
        }

        dialog.emit_close();
    }

    fn queue_quit(&self) {
        *self.shutdown_in_progress.write() = true;
        self.get_main_window().widget.close();
        self.get_main_window().execute_pending_undoable_action();

        // wait for ongoing sync to finish, but limit waiting to max 3s
        let start_wait_time = time::SystemTime::now();
        let max_wait_time = time::Duration::from_secs(3);

        while Self::is_syncing(&self.news_flash)
            && start_wait_time.elapsed().expect("shutdown timer elapsed error") < max_wait_time
        {
            gtk::main_iteration();
        }

        Util::send(&self.sender, Action::ForceQuit);
    }

    fn force_quit(&self) {
        info!("Shutdown!");
        self.as_app().quit();
    }

    fn is_syncing(news_flash: &Arc<RwLock<Option<NewsFlash>>>) -> bool {
        if let Some(news_flash) = news_flash.read().as_ref() {
            if news_flash.is_sync_ongoing() {
                return true;
            }
        }
        false
    }

    fn set_offline(&self, offline: bool) {
        self.get_main_window().state.write().set_offline(offline);
        self.get_main_window().content_header.set_offline(offline);
        self.get_main_window().content_page.sidebar.read().footer.update();
        self.get_main_window()
            .content_page
            .sidebar
            .read()
            .feed_list
            .read()
            .update_offline();
    }

    fn ignore_tls_errors(&self) {
        if self.settings.write().set_accept_invalid_certs(true).is_err() {
            Util::send(
                &self.sender,
                Action::ErrorSimpleMessage("Error writing settings.".to_owned()),
            );
        }
        if self.settings.write().set_accept_invalid_hostnames(true).is_err() {
            Util::send(
                &self.sender,
                Action::ErrorSimpleMessage("Error writing settings.".to_owned()),
            );
        }
    }

    fn open_selected_article_in_browser(&self) {
        let article_model = self.get_main_window().content_page.article_view.get_visible_article();
        if let (Some(article_model), _article_enclosures) = article_model {
            if let Some(url) = article_model.url {
                Util::send(&self.sender, Action::OpenUrlInDefaultBrowser(url.to_string()));
            } else {
                warn!("Open selected article in browser: No url available.")
            }
        } else {
            warn!("Open selected article in browser: No article Selected.")
        }
    }

    fn open_url_in_default_browser(&self, url: String) {
        let result = open::that(&url);

        if let Err(error) = result {
            Util::send(
                &self.sender,
                Action::ErrorSimpleMessage(format!("Failed to open URL: {}", error)),
            );
        }
    }

    fn query_disk_space(&self, oneshot_sender: OneShotSender<UserDataSize>) {
        if let Some(news_flash) = self.news_flash.read().as_ref() {
            if let Ok(db_size) = news_flash.database_size() {
                if let Ok(webkit_size) = news_flash::util::folder_size(&WEBKIT_DATA_DIR) {
                    let user_data_size = UserDataSize {
                        database: db_size,
                        webkit: webkit_size,
                    };
                    oneshot_sender.send(user_data_size).expect(CHANNEL_ERROR);
                }
            }
        }
    }

    fn clear_cache(&self, oneshot_sender: OneShotSender<()>) {
        self.get_main_window()
            .content_page
            .article_view
            .clear_cache(oneshot_sender);
    }
}

glib_wrapper! {
    pub struct App(Object<subclass::simple::InstanceStruct<AppPrivate>, subclass::simple::ClassStruct<AppPrivate>, AppClass>)
        @extends gio::Application, gtk::Application;

    match fn {
        get_type => || AppPrivate::get_type().to_glib(),
    }
}

impl App {
    pub fn run(allow_webview_inspector: bool, start_headless: bool) {
        let app = glib::Object::new(
            App::static_type(),
            &[("application-id", &Some(APP_ID)), ("flags", &ApplicationFlags::empty())],
        )
        .expect("Failed to create new App gobject")
        .downcast::<App>()
        .expect("Failed to cast to App");

        let private = AppPrivate::from_instance(&app);
        private
            .settings
            .write()
            .set_inspect_article_view(allow_webview_inspector);
        *private.start_headless.write() = start_headless;
        ApplicationExtManual::run(&app, &vec![]);
    }

    fn process_action(&self, action: Action) -> glib::Continue {
        let private = AppPrivate::from_instance(self);

        match action {
            Action::ShowNotification(counts) => private.show_notification(counts),
            Action::ErrorSimpleMessage(msg) => private.get_main_window().show_error_simple_message(&msg),
            Action::Error(msg, error) => private.get_main_window().show_error(&msg, error),
            Action::UndoableAction(action) => private.get_main_window().show_undo_bar(action),
            Action::LoadFavIcon((feed_id, sender)) => private.load_favicon(feed_id, sender),
            Action::LoadThumbnail((article_id, sender)) => private.load_thumbnail(article_id, sender),
            Action::ShowWelcomePage => private.get_main_window().show_welcome_page(),
            Action::ShowContentPage(plugin_id) => {
                private
                    .get_main_window()
                    .show_content_page(&plugin_id, &private.news_flash, &private.features)
            }
            Action::ShowPasswordLogin(plugin_id, data, prev_page) => private
                .get_main_window()
                .show_password_login_page(&plugin_id, data, prev_page),
            Action::ShowOauthLogin(plugin_id, prev_page) => {
                private.get_main_window().show_oauth_login_page(&plugin_id, prev_page)
            }
            Action::ShowResetPage => private.get_main_window().show_reset_page(),
            Action::ShowDiscoverDialog => private.spawn_discover_dialog(),
            Action::ShowSettingsWindow => private.spawn_settings_window(),
            Action::ShowShortcutWindow => private.spawn_shortcut_window(),
            Action::ShowAboutWindow => private.spawn_about_window(),
            Action::CancelReset => private.get_main_window().cancel_reset(),
            Action::Login(data) => private.login(data),
            Action::UpdateLogin => private.update_login(),
            Action::ResetAccount => private.reset_account(),
            Action::ResetAccountError(error) => private.get_main_window().reset_account_failed(error),
            Action::ScheduleSync => private.schedule_sync(),
            Action::Sync => private.sync(),
            Action::InitSync => private.init_sync(),
            Action::MarkArticleRead(update) => private.mark_article_read(update),
            Action::MarkArticle(update) => private.mark_article(update),
            Action::ToggleArticleRead => private.toggle_article_read(),
            Action::ToggleArticleMarked => private.toggle_article_marked(),
            Action::UpdateSidebar => private.get_main_window().update_sidebar(
                &private.news_flash,
                private.threadpool.clone(),
                &private.features,
            ),
            Action::UpdateArticleList => private
                .get_main_window()
                .update_article_list(&private.news_flash, private.threadpool.clone()),
            Action::ArticleListIndexSelected(index) => private
                .get_main_window()
                .content_page
                .article_list
                .read()
                .index_selected(index),
            Action::LoadMoreArticles => private
                .get_main_window()
                .load_more_articles(&private.news_flash, private.threadpool.clone()),
            Action::SidebarSelection(selection) => private.get_main_window().sidebar_selection(selection),
            Action::SelectNextArticle => private
                .get_main_window()
                .content_page
                .article_list
                .read()
                .select_next_article(),
            Action::SelectPrevArticle => private
                .get_main_window()
                .content_page
                .article_list
                .read()
                .select_prev_article(),
            Action::HeaderSelection(selection) => private.get_main_window().set_headerbar_selection(selection),
            Action::UpdateArticleHeader => private
                .get_main_window()
                .update_article_header(&private.news_flash, &private.features),
            Action::ShowArticle(article_id) => {
                private
                    .get_main_window()
                    .show_article(article_id, &private.news_flash, &private.features)
            }
            Action::RedrawArticle => private.get_main_window().content_page.article_view.redraw_article(),
            Action::CloseArticle => {
                private.get_main_window().content_page.article_view.close_article();
                private.get_main_window().content_header.show_article(
                    None,
                    None,
                    &private.news_flash,
                    &private.features,
                );
            }
            Action::SearchTerm(search_term) => private.get_main_window().set_search_term(search_term),
            Action::SetSidebarRead => private.get_main_window().set_sidebar_read(
                &private.news_flash,
                private.threadpool.clone(),
                private.settings.clone(),
            ),
            Action::AddDialog => private.add_feed_dialog(),
            Action::AddFeed((url, title, category)) => private.add_feed(url, title, category),
            Action::AddCategory(title) => private.add_category(title),
            Action::AddTag(color, title) => private.add_tag(color, title),
            Action::RenameFeedDialog(feed_id, category_id) => private.rename_feed_dialog(feed_id, category_id),
            Action::RenameFeed((feed, new_title)) => private.rename_feed(feed, new_title),
            Action::RenameCategoryDialog(category_id) => private.rename_category_dialog(category_id),
            Action::RenameCategory((category, new_title)) => private.rename_category(category, new_title),
            Action::DeleteSidebarSelection => private.delete_selection(),
            Action::DeleteFeed(feed_id) => private.delete_feed(feed_id),
            Action::DeleteCategory(category_id) => private.delete_category(category_id),
            Action::DeleteTag(tag_id) => private.delete_tag(tag_id),
            Action::TagArticle(article_id, tag_id) => private.tag_article(article_id, tag_id),
            Action::UntagArticle(article_id, tag_id) => private.untag_article(article_id, tag_id),
            Action::DragAndDrop(action) => private.drag_and_drop(action),
            Action::ExportArticle => private.export_article(),
            Action::StartGrabArticleContent => private.start_grab_article_content(),
            Action::MercuryGrabArticleContent(article, next_page_url, previous_urls) => {
                private.mercury_grab_article_contnet(article, next_page_url, previous_urls)
            }
            Action::FinishGrabArticleContent(article) => private.finish_grab_article_content(article),
            Action::ImportOpml => private.import_opml(),
            Action::ExportOpml => private.export_opml(),
            Action::QueueQuit => private.queue_quit(),
            Action::ForceQuit => private.force_quit(),
            Action::SetOfflineMode(offline) => private.set_offline(offline),
            Action::IgnoreTLSErrors => private.ignore_tls_errors(),
            Action::OpenSelectedArticle => private.open_selected_article_in_browser(),
            Action::OpenUrlInDefaultBrowser(url) => private.open_url_in_default_browser(url),
            Action::QueryDiskSpace(oneshot) => private.query_disk_space(oneshot),
            Action::ClearCache(oneshot) => private.clear_cache(oneshot),
        }
        glib::Continue(true)
    }
}
